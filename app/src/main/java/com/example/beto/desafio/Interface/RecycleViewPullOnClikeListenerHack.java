package com.example.beto.desafio.Interface;

import android.view.View;

/**
 * Created by Beto on 02/02/2018.
 */

public interface RecycleViewPullOnClikeListenerHack {

    void onClickListener(View view, int position);

}
