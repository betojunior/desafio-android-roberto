package com.example.beto.desafio.View.AdapterPull;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beto.desafio.R;

/**
 * Created by Beto on 30/01/2018.
 */

public class ListPullViewHolder extends RecyclerView.ViewHolder {

    /*
    * Componentes da tela
    */
    TextView txtTitulo;
    TextView txtDesc;
    ImageView imageView;
    TextView txtUserName;
    TextView txtNome;

    public ListPullViewHolder(View itemView) {
        super(itemView);

        txtTitulo = (TextView) itemView.findViewById(R.id.txtTitulo);
        txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
        imageView = (ImageView) itemView.findViewById(R.id.imageView);
        txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
        txtNome = (TextView) itemView.findViewById(R.id.txtNome);
    }
}
