package com.example.beto.desafio.View;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.example.beto.desafio.Interface.RecycleViewOnClikeListenerHack;
import com.example.beto.desafio.Interface.ReqGitHub;
import com.example.beto.desafio.Models.Repositorio;
import com.example.beto.desafio.R;
import com.example.beto.desafio.Repo.RepoGitHubAPI;
import com.example.beto.desafio.Uteis.EndlessRecyclerViewScrollListener;
import com.example.beto.desafio.Uteis.StatusConexao;
import com.example.beto.desafio.View.AdapterRepo.RepoAdapter;
import com.example.beto.desafio.View.activity.PullsActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class homeFragment extends Fragment implements RecycleViewOnClikeListenerHack {
    public static final String LISTREPO = "LISTREPO";
    private RecyclerView recyclerView;
    private RepoAdapter repoAdapter;
    private Context context;
    private List<Repositorio> repositorios;
    private ProgressBar progressBar;
    private SwipeRefreshLayout refreshLayout;
    private EndlessRecyclerViewScrollListener scrollListener;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(repoAdapter!=null){
            outState.putParcelableArrayList(LISTREPO, (ArrayList<? extends Parcelable>) repoAdapter.getListRepo());
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            repositorios = savedInstanceState.getParcelableArrayList(LISTREPO);

        } else {
            getListRepositorios("1");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInsatanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        context = getContext();

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refresh);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!progressBar.isEnabled()) {
                    getListRepositorios("1");
                    refreshLayout.setRefreshing(false);
                }
                refreshLayout.setRefreshing(false);
            }
        });

        if (repositorios != null && repositorios.size() > 0) {
            carregarAdapter(repositorios, view);
        } else {
            progressBar.setIndeterminate(true);
        }

        return view;

    }

    private void getListRepositorios(String page) {
        if(StatusConexao.verificaConexao(getContext())){
            RepoGitHubAPI requestRepositorios = new RepoGitHubAPI();

            requestRepositorios.getRepositorios(new ReqGitHub.ReqRepositorios() {
                @Override
                public void listaRepositorios(List<Repositorio> reqRepositorios) {

                    if (reqRepositorios != null && reqRepositorios.size() > 0) {

                        if (repositorios != null) {
                            repoAdapter.insertItems(reqRepositorios);
                            progressBar.setEnabled(false);
                            progressBar.setVisibility(View.INVISIBLE);
                        } else {
                            repositorios = reqRepositorios;
                            carregarAdapter(reqRepositorios, getView());
                        }

                    } else {
                        progressBar.setEnabled(false);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }, getContext(), page);
        }else {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), R.string.verificar_conexao,Toast.LENGTH_LONG).show();
                }
            });

            if(progressBar!=null){
                progressBar.setEnabled(false);
                progressBar.setVisibility(View.INVISIBLE);
            }
        }



    }

    private void carregarAdapter(List<Repositorio> aListRepo, View view) {

        LinearLayoutManager lln = new LinearLayoutManager(getActivity());
        lln.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) view.findViewById(R.id.rv_repo);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lln);

        repoAdapter = new RepoAdapter(aListRepo, context);
        repoAdapter.setRecyclerViewOnClick(this);
        recyclerView.setAdapter(repoAdapter);

        recyclerView.addItemDecoration(
                new DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        );

        scrollListener = new EndlessRecyclerViewScrollListener(lln) {
            @Override
            public void onLoadMore(final int page, int totalItemsCount, RecyclerView view) {
                //carregar mais paginas
                progressBar.setEnabled(true);
                progressBar.setVisibility(View.VISIBLE);

                getListRepositorios(String.valueOf(page + 1));

            }
        };

        recyclerView.addOnScrollListener(scrollListener);

        progressBar.setEnabled(false);
        progressBar.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onClickListener(View view, int position) {
        Intent intent = new Intent(context, PullsActivity.class);
        intent.putExtra("login", repoAdapter.getRepo(position).getUser().getLogin());
        intent.putExtra("name", repoAdapter.getRepo(position).getName());

        startActivity(intent);
    }

    @Override
    public void onBindViewHolder(final RepoAdapter.MyViewHolder holder, final int position) {

    }

}
