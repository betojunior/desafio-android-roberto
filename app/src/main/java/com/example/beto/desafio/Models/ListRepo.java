package com.example.beto.desafio.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Beto on 31/01/2018.
 */

public class ListRepo implements Parcelable {
    @SerializedName("items")
    private List<Repositorio> items;

    public List<Repositorio> getItems() {
        return items;
    }

    public void setItems(List<Repositorio> items) {
        this.items = items;
    }


    protected ListRepo(Parcel in) {
        if (in.readByte() == 0x01) {
            items = new ArrayList<Repositorio>();
            in.readList(items, Repositorio.class.getClassLoader());
        } else {
            items = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (items == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(items);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ListRepo> CREATOR = new Parcelable.Creator<ListRepo>() {
        @Override
        public ListRepo createFromParcel(Parcel in) {
            return new ListRepo(in);
        }

        @Override
        public ListRepo[] newArray(int size) {
            return new ListRepo[size];
        }
    };
}