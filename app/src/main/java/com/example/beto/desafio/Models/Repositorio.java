package com.example.beto.desafio.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Beto on 30/01/2018.
 */

public class Repositorio implements Parcelable {

    private String name;
    private String full_name;
    private String description;
    @SerializedName("owner")
    private Usuario user;
    private int stargazers_count;
    private int forks_count;
    private String url;

    public Repositorio(String name, String description, Usuario owner, int stargazers_count, int forks_count){
        this.name = name;
        this.description = description;
        this.user = owner;
        this.stargazers_count = stargazers_count;
        this.forks_count = forks_count;
    }

    public Repositorio(){
        this.user = new Usuario();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public int getStargazers_count() {
        return stargazers_count;
    }

    public void setStargazers_count(int stargazers_count) {
        this.stargazers_count = stargazers_count;
    }

    public int getForks_count() {
        return forks_count;
    }

    public void setForks_count(int forks_count) {
        this.forks_count = forks_count;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    protected Repositorio(Parcel in) {
        name = in.readString();
        full_name = in.readString();
        description = in.readString();
        user = (Usuario) in.readValue(Usuario.class.getClassLoader());
        stargazers_count = in.readInt();
        forks_count = in.readInt();
        url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(full_name);
        dest.writeString(description);
        dest.writeValue(user);
        dest.writeInt(stargazers_count);
        dest.writeInt(forks_count);
        dest.writeString(url);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Repositorio> CREATOR = new Parcelable.Creator<Repositorio>() {
        @Override
        public Repositorio createFromParcel(Parcel in) {
            return new Repositorio(in);
        }

        @Override
        public Repositorio[] newArray(int size) {
            return new Repositorio[size];
        }
    };
}
