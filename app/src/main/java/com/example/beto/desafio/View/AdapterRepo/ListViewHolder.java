package com.example.beto.desafio.View.AdapterRepo;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.beto.desafio.R;

/**
 * Created by Beto on 30/01/2018.
 */

public class ListViewHolder extends RecyclerView.ViewHolder {
    /*
    * Componentes da tela
    */
    TextView txtNomeRepositorio;
    TextView txtDescricao;
    TextView txtUserName;
    TextView txtNome;
    TextView txtFork;
    TextView txtLike;


    public ListViewHolder(View itemView) {
        super(itemView);

        txtNomeRepositorio = (TextView) itemView.findViewById(R.id.txtNomeRepositorio);
        txtDescricao = (TextView) itemView.findViewById(R.id.txtDescricao);
        txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
        txtNome = (TextView) itemView.findViewById(R.id.txtNome);
        txtFork = (TextView) itemView.findViewById(R.id.txtFork);
        txtLike = (TextView) itemView.findViewById(R.id.txtLike);

    }
}
