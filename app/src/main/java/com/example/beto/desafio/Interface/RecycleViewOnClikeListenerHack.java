package com.example.beto.desafio.Interface;

import android.view.View;

import com.example.beto.desafio.View.AdapterRepo.RepoAdapter;


public interface RecycleViewOnClikeListenerHack {

    void onClickListener(View view, int position);

    void onBindViewHolder(final RepoAdapter.MyViewHolder holder, final int position);


}
