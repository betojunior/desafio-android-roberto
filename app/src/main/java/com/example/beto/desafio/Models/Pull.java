package com.example.beto.desafio.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Beto on 30/01/2018.
 */

public class Pull implements Parcelable {
    private String title;
    private String full_name;
    @SerializedName("body")
    private String description;
    private String html_url;
    @SerializedName("created_at")
    private Date date;
    private Usuario user;

    public Pull(){
        this.user = new Usuario();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getHtml_url() {
        return html_url;
    }

    public void setHtml_url(String html_url) {
        this.html_url = html_url;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }


    protected Pull(Parcel in) {
        title = in.readString();
        full_name = in.readString();
        description = in.readString();
        html_url = in.readString();
        long tmpDate = in.readLong();
        date = tmpDate != -1 ? new Date(tmpDate) : null;
        user = (Usuario) in.readValue(Usuario.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(full_name);
        dest.writeString(description);
        dest.writeString(html_url);
        dest.writeLong(date != null ? date.getTime() : -1L);
        dest.writeValue(user);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Pull> CREATOR = new Parcelable.Creator<Pull>() {
        @Override
        public Pull createFromParcel(Parcel in) {
            return new Pull(in);
        }

        @Override
        public Pull[] newArray(int size) {
            return new Pull[size];
        }
    };
}