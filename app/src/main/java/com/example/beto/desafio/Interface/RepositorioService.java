package com.example.beto.desafio.Interface;

import com.example.beto.desafio.Models.ListRepo;
import com.example.beto.desafio.Models.Pull;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Beto on 30/01/2018.
 */

public interface RepositorioService {
    @GET("search/repositories?q=language:Java&sort=&page=")
    Call<ListRepo>listRepos(@Query("page") String page, @Query("sort") String sort );

    @GET("repos/{login}/{name}/pulls")
    Call<List<Pull>>listPulls(@Path("login") String full_name , @Path("name") String name  );

}
