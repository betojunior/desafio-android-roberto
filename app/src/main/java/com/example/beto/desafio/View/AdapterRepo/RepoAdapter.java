package com.example.beto.desafio.View.AdapterRepo;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.ceylonlabs.imageviewpopup.ImagePopup;
import com.example.beto.desafio.Interface.RecycleViewOnClikeListenerHack;
import com.example.beto.desafio.Models.Repositorio;
import com.example.beto.desafio.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.MyViewHolder> {

    private List<Repositorio> mListaRepo;
    private LayoutInflater mLayoutInflater;
    private RecycleViewOnClikeListenerHack recycleViewOnClikeListenerHack;

    private Context context;

    public RepoAdapter(List<Repositorio> mListaReceica, Context context) {
        this.mListaRepo = mListaReceica;
        this.context = context;
        Animation animation = new TranslateAnimation(50,0,0,0);
        animation.setFillAfter(true);
        animation.setDuration(2000);
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtNomeRepositorio;
        TextView txtDescricao;
        TextView txtUserName;
        TextView txtNome;
        TextView txtFork;
        TextView txtLike;
        ImageView imageView;

        public View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtNomeRepositorio = (TextView) itemView.findViewById(R.id.txtNomeRepositorio);
            txtDescricao = (TextView) itemView.findViewById(R.id.txtDescricao);
            txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
            txtNome = (TextView) itemView.findViewById(R.id.txtNome);
            txtFork = (TextView) itemView.findViewById(R.id.txtFork);
            txtLike = (TextView) itemView.findViewById(R.id.txtLike);
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(this);
            view = itemView;
        }


        @Override
        public void onClick(View v) {
            if (recycleViewOnClikeListenerHack != null) {
                recycleViewOnClikeListenerHack.onClickListener(v, getAdapterPosition());

            }
        }
    }

    public void setRecyclerViewOnClick(RecycleViewOnClikeListenerHack recycleViewOnClikeListenerHack) {
        this.recycleViewOnClikeListenerHack = recycleViewOnClikeListenerHack;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mLayoutInflater.inflate(R.layout.linha_repo, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final RepoAdapter.MyViewHolder holder, int position) {
        holder.txtNomeRepositorio.setText(mListaRepo.get(position).getName());
        holder.txtDescricao.setText(mListaRepo.get(position).getDescription());
        holder.txtUserName.setText(mListaRepo.get(position).getUser().getLogin());
        holder.txtNome.setText(mListaRepo.get(position).getFull_name());
        holder.txtLike.setText(String.valueOf(mListaRepo.get(position).getStargazers_count()));
        holder.txtFork.setText(String.valueOf(mListaRepo.get(position).getForks_count()));

        Picasso.with(context)
                .load(mListaRepo.get(position).getUser().getAvatar_url())
                .into(holder.imageView)
        ;

        final ImagePopup imagePopup = new ImagePopup(context);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imagePopup.initiatePopup(holder.imageView.getDrawable());
                imagePopup.viewPopup();
            }
        });
    }



    @Override
    public void onBindViewHolder( MyViewHolder holder, final int position, List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);

    }

    @Override
    public int getItemCount() {
        return mListaRepo.size();
    }

    public void insertItems(List<Repositorio> repos) {
        int i = repos.size();
        mListaRepo.addAll(repos);
        notifyItemRangeInserted(mListaRepo.size()-repos.size(),mListaRepo.size());
    }
    public Repositorio getRepo(int positon) {
        return mListaRepo.get(positon);
    }

    private void updateItem(int position) {
        mListaRepo.get(position);
        notifyItemChanged(position);
    }

    private void removerItem(int position) {
        mListaRepo.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mListaRepo.size());
    }

    public void updateList(List<Repositorio> repos) {
        insertItems(repos);
    }

    public List<Repositorio> getListRepo() {
        return mListaRepo;
    }



}