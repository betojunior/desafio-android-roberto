package com.example.beto.desafio.Repo;

import android.content.Context;

import com.example.beto.desafio.Connection.RetrofitGitHub;
import com.example.beto.desafio.Interface.RepositorioService;
import com.example.beto.desafio.Interface.ReqGitHub;
import com.example.beto.desafio.Models.ListRepo;
import com.example.beto.desafio.Models.Pull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Beto on 31/01/2018.
 */

public class RepoGitHubAPI {

    public void getRepositorios(final ReqGitHub.ReqRepositorios reqRepositorios, Context context, String pagina) {

        RepositorioService reqGitHub = RetrofitGitHub.getRetrofit(context).create(RepositorioService.class);
        Call<ListRepo> listRepoCall = reqGitHub.listRepos(pagina, "stars");

        listRepoCall.enqueue(new Callback<ListRepo>() {
            @Override
            public void onResponse(Call<ListRepo> call, Response<ListRepo> response) {
                ListRepo listRepo = response.body();

                if (listRepo != null && listRepo.getItems() != null) {
                    reqRepositorios.listaRepositorios(listRepo.getItems());
                }

            }

            @Override
            public void onFailure(Call<ListRepo> call, Throwable t) {
                reqRepositorios.listaRepositorios(null);
            }
        });
    }

    public void getPulls(final ReqGitHub.ReqPulls reqPulls, Context context, String login, String nameRepo) {

        RepositorioService reqGitHub = RetrofitGitHub.getRetrofit(context).create(RepositorioService.class);
        Call<List<Pull>> listRepoCall = reqGitHub.listPulls(login, nameRepo);

        listRepoCall.enqueue(new Callback<List<Pull>>() {
            @Override
            public void onResponse(Call<List<Pull>> call, Response<List<Pull>> response) {
                List<Pull> listPull = response.body();

                if (listPull != null && listPull.size() > 0) {
                    reqPulls.listaReqPulls(listPull);
                }
            }

            @Override
            public void onFailure(Call<List<Pull>> call, Throwable t) {
                reqPulls.listaReqPulls(null);
            }
        });
    }


}
