package com.example.beto.desafio.View.AdapterPull;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.beto.desafio.Interface.RecycleViewPullOnClikeListenerHack;
import com.example.beto.desafio.Models.Pull;
import com.example.beto.desafio.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PullAdapter extends RecyclerView.Adapter<PullAdapter.MyViewHolderPull> {

    private List<Pull> mListPulls;
    private LayoutInflater mLayoutInflater;
    private RecycleViewPullOnClikeListenerHack recycleViewOnClikeListenerHack;
    private Context context;

    public PullAdapter(List<Pull> mListaReceica, Context context) {
        this.mListPulls = mListaReceica;
        this.context = context;
        Animation animation = new TranslateAnimation(50,0,0,0);
        animation.setFillAfter(true);
        animation.setDuration(2000);
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public class MyViewHolderPull extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView txtTitulo;
        TextView txtDesc;
        ImageView imageView;
        TextView txtUserName;
        TextView txtDate;

        public View view;

        public MyViewHolderPull(View itemView) {
            super(itemView);

            txtTitulo = (TextView) itemView.findViewById(R.id.txtTitulo);
            txtDesc = (TextView) itemView.findViewById(R.id.txtDesc);
            imageView = (ImageView) itemView.findViewById(R.id.imageViewPull);
            txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
            txtDate = (TextView) itemView.findViewById(R.id.txtDate);

            itemView.setOnClickListener(this);
            view = itemView;
        }


        @Override
        public void onClick(View v) {
            if (recycleViewOnClikeListenerHack != null) {
                recycleViewOnClikeListenerHack.onClickListener(v, getAdapterPosition());

            }
        }
    }

    public void setRecyclerViewOnClick(RecycleViewPullOnClikeListenerHack recycleViewOnClikeListenerHack) {
        this.recycleViewOnClikeListenerHack = recycleViewOnClikeListenerHack;
    }


    @Override
    public MyViewHolderPull onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.linha_pull, parent, false);
        MyViewHolderPull myViewHolder = new MyViewHolderPull(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolderPull holder, int position) {

        Picasso.with(context)
                .load(mListPulls.get(position).getUser().getAvatar_url())
                .into(holder.imageView);
        holder.txtTitulo.setText(mListPulls.get(position).getTitle());
        holder.txtDesc.setText(mListPulls.get(position).getDescription());
        holder.txtUserName.setText(mListPulls.get(position).getUser().getLogin());
        holder.txtDate.setText(mListPulls.get(position).getDate().toString());
    }

    @Override
    public int getItemCount() {
        return mListPulls.size();
    }

    public void insertItems(List<Pull> repos) {
        int i = repos.size();
        mListPulls.addAll(repos);
        notifyItemRangeInserted(mListPulls.size()-repos.size(), mListPulls.size());
    }
    public Pull getRepo(int positon) {
        return mListPulls.get(positon);
    }

    private void updateItem(int position) {
        mListPulls.get(position);
        notifyItemChanged(position);
    }

    private void removerItem(int position) {
        mListPulls.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mListPulls.size());
    }

    public void updateList(List<Pull> repos) {
        insertItems(repos);
    }

    public List<Pull> getPulls() {
        return mListPulls;
    }


}