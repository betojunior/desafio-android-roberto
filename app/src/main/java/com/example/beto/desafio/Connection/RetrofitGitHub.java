package com.example.beto.desafio.Connection;

import android.content.Context;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Beto on 31/01/2018.
 */

public class RetrofitGitHub {

    private static Retrofit retrofit;
    private static int cacheSize = 10 * 1024 * 1024;

    public static Retrofit getRetrofit(Context context) {

        if (retrofit == null) {
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .cache(new Cache(context.getCacheDir(), cacheSize))
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.github.com")
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retrofit;
    }


}
