package com.example.beto.desafio.View.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.beto.desafio.Interface.RecycleViewPullOnClikeListenerHack;
import com.example.beto.desafio.Interface.ReqGitHub;
import com.example.beto.desafio.Models.Pull;
import com.example.beto.desafio.R;
import com.example.beto.desafio.Repo.RepoGitHubAPI;
import com.example.beto.desafio.Uteis.StatusConexao;
import com.example.beto.desafio.View.AdapterPull.PullAdapter;

import java.util.ArrayList;
import java.util.List;

public class PullsActivity extends AppCompatActivity implements RecycleViewPullOnClikeListenerHack {

    public static final String LISTPULL = "LISTPULL";
    private RecyclerView recyclerView;
    private PullAdapter pullAdapter;
    private Toolbar myToolbar;
    private Context context;
    private List<Pull> listPulls;
    private ProgressBar progressBar;
    private SwipeRefreshLayout refreshLayout;
    private String login;
    private String name;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (pullAdapter!=null){
            outState.putParcelableArrayList(LISTPULL, (ArrayList<? extends Parcelable>) pullAdapter.getPulls());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pulls);

        context = this;

        myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        login = getIntent().getStringExtra("login");
        name = getIntent().getStringExtra("name");

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                listPulls = null;
                getListPulls(login, name);
                refreshLayout.setRefreshing(false);
            }
        });

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            if (login != null && name != null) {
                getListPulls(login, name);
            }
        } else {
            listPulls = savedInstanceState.getParcelableArrayList(LISTPULL);
            if (listPulls.size() > 0) {
                carregarAdapter(listPulls);
            }

        }


    }

    private void getListPulls(String login, String name) {

        if (StatusConexao.verificaConexao(getApplication())) {
            RepoGitHubAPI requestRepositorios = new RepoGitHubAPI();
            requestRepositorios.getPulls(new ReqGitHub.ReqPulls() {
                @Override
                public void listaReqPulls(List<Pull> pulls) {
                    if (pulls != null && pulls.size() > 0) {

                        if (listPulls != null) {
                            pullAdapter.insertItems(pulls);
                            progressBar.setEnabled(false);
                            progressBar.setVisibility(View.INVISIBLE);
                        } else {
                            listPulls = pulls;
                            carregarAdapter(listPulls);
                        }

                    } else {
                        progressBar.setEnabled(false);
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }
            }, getApplicationContext(), login, name);

        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), R.string.verificar_conexao,Toast.LENGTH_LONG).show();
                }
            });
            if(progressBar!=null){
                progressBar.setEnabled(false);
                progressBar.setVisibility(View.INVISIBLE);
            }

        }

    }


    private void carregarAdapter(List<Pull> aListPull) {

        LinearLayoutManager lln = new LinearLayoutManager(context);
        lln.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = (RecyclerView) findViewById(R.id.rv_pull);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(lln);

        pullAdapter = new PullAdapter(aListPull, context);
        pullAdapter.setRecyclerViewOnClick(this);
        recyclerView.setAdapter(pullAdapter);

        recyclerView.addItemDecoration(
                new DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        );


        //recyclerView.addOnScrollListener(scrollListener);
        progressBar.setEnabled(false);
        progressBar.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onClickListener(View view, int position) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(pullAdapter.getRepo(position).getHtml_url()));
        startActivity(browserIntent);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }


}
