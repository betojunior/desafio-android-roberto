package com.example.beto.desafio.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Beto on 30/01/2018.
 */

public class Usuario implements Parcelable {

    private String login;
    private String avatar_url;

    public Usuario(String login, String avatar_url){
        this.login = login;
        this.avatar_url = avatar_url;
    }
    public Usuario(){

    }
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatar_url() {
        return avatar_url;
    }

    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }


    protected Usuario(Parcel in) {
        login = in.readString();
        avatar_url = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(login);
        dest.writeString(avatar_url);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Usuario> CREATOR = new Parcelable.Creator<Usuario>() {
        @Override
        public Usuario createFromParcel(Parcel in) {
            return new Usuario(in);
        }

        @Override
        public Usuario[] newArray(int size) {
            return new Usuario[size];
        }
    };
}
