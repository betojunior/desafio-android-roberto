package com.example.beto.desafio.Interface;

import com.example.beto.desafio.Models.Pull;
import com.example.beto.desafio.Models.Repositorio;

import java.util.List;

/**
 * Created by Beto on 31/01/2018.
 */

public interface ReqGitHub {

    public interface ReqRepositorios{
        void listaRepositorios(List<Repositorio> repositorios);
    }

    public interface ReqPulls{
        void listaReqPulls(List<Pull> pulls);
    }

}
